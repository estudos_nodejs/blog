const app = require('./app');

require('dotenv').config({ path: 'variables.env' });

const { PORT } = process.env || 7777;

const server = app.listen(PORT, () => {
  console.log(`Servidor rodando na porta ${PORT}`);
});
